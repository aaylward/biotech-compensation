import pandas as pd
import sys
import seaborn as sns
from statistics import median
from collections import Counter
def parse_bonus(base, bonus):
    try:
        return float(bonus.replace(',', ''))
    except ValueError:
        return 0.0 if bonus == 'Not Applicable/None' else float(bonus.split('%')[0])/100 * base

def main():
    r_biotech_2022 = pd.DataFrame(row for _, row in pd.read_table('r_biotech_2022.tsv').iterrows()
        if row['Where is the closest major city or hub?']
        in {'San Diego', 'San Diego, CA', 'San Diego '}).iloc[:, [1, 2, 4, 5, 6, 7, 8, 9, 10]]
    r_biotech_2022['Parsed bonus'] = tuple(parse_bonus(base, bonus) for _, (base, bonus) in r_biotech_2022.iloc[:,7:9].iterrows())
    r_biotech_2022['Compensation - Annual Pay inc. Bonus'] = r_biotech_2022['Compensation - Annual Base Salary/Pay'] + r_biotech_2022['Parsed bonus']
    plotting_data = r_biotech_2022.loc[:,['Years of Experience', 'Compensation - Annual Base Salary/Pay', 'Compensation - Annual Pay inc. Bonus']]
    ax = sns.regplot(x='Years of Experience', y='Compensation - Annual Pay inc. Bonus', data=plotting_data, color=sns.color_palette('pastel')[0])
    ax = sns.regplot(x='Years of Experience', y='Compensation - Annual Base Salary/Pay', data=plotting_data)
    ax.hlines(y=median(plotting_data['Compensation - Annual Pay inc. Bonus']), xmin=0, xmax=20, linestyles='dashed', color=sns.color_palette('pastel')[0])
    ax.hlines(y=median(plotting_data['Compensation - Annual Base Salary/Pay']), xmin=0, xmax=20, linestyles='dashed')
    ax.set_title('San Diego, 2022')
    fig = ax.get_figure()
    fig.savefig('r_biotech_2022.svg')
    fig.clf()

    r_biotech_2023 = pd.read_table('r_biotech_2023.tsv')
    r_biotech_2023 = pd.DataFrame(row for _, row in pd.read_table('r_biotech_2023.tsv').iterrows()
        if row['Where is the closest major city or hub?']
        in {'San Diego', 'San Diego, CA', 'San Diego '}).iloc[:, [2, 3, 5, 6, 7, 8, 11, 12, 14]]
    r_biotech_2023['Parsed bonus'] = tuple(parse_bonus(base, bonus) for _, (base, bonus) in r_biotech_2023.iloc[:,7:9].iterrows())
    r_biotech_2023['Compensation - Annual Pay inc. Bonus'] = r_biotech_2023['Compensation - Annual Base Salary/Pay'] + r_biotech_2023['Parsed bonus']
    plotting_data = r_biotech_2023.loc[:,['Years of Experience', 'Compensation - Annual Base Salary/Pay', 'Compensation - Annual Pay inc. Bonus']]
    ax = sns.regplot(x='Years of Experience', y='Compensation - Annual Pay inc. Bonus', data=plotting_data, color=sns.color_palette('pastel')[1])
    ax = sns.regplot(x='Years of Experience', y='Compensation - Annual Base Salary/Pay', data=plotting_data, color=sns.color_palette()[1])
    ax.hlines(y=median(plotting_data['Compensation - Annual Pay inc. Bonus']), xmin=0, xmax=10, linestyles='dashed', color=sns.color_palette('pastel')[1])
    ax.hlines(y=median(plotting_data['Compensation - Annual Base Salary/Pay']), xmin=0, xmax=10, linestyles='dashed', color=sns.color_palette()[1])
    ax.set_title('San Diego, 2023')
    fig = ax.get_figure()
    fig.savefig('r_biotech_2023.svg')
    fig.clf()

if __name__ == '__main__':
    main()
