# Biotech compensation benchmarks

## Pillar VC biotech compensation analysis

Pillar VC [collected data](https://founderledbio.com/biotech-compensation-study/) on biotech compensation:

![about](pillar-about.png)
![vital stats](pillar-vital-stats.png)
![engineering](pillar-engineering.png)
![scientist](pillar-scientist.png)

## Reddit biotech compensation survey

The [r/biotech](https://www.reddit.com/r/biotech/) subreddit conducted a
[survey](https://docs.google.com/spreadsheets/d/1G0FmJhkOME_sv66hWmhnZS5qR2KMTY7nzkxksv46bfk/edit#gid=453658888) of compensation statistics. This dataset includes responses
from San Diego specifically. Dark shaded data points indicate base pay,
light shaded points indicate base pay + bonus.

![2022](r_biotech_2022.svg)
![2023](r_biotech_2023.svg)
